# AIF Access and Reminder Workflow

To the end of automating the notification and approval process associated with expiring agreements, this SharePoint 2013 workflow saved an employee in the Legislative department 5 hours a week. A few functions include setting item-level access, emailing notifications, and managing approval tasks.

https://trello.com/b/0CRQBTBK