SETUP

Grant the workflow service administrative rights:
https://docs.microsoft.com/en-us/sharepoint/dev/general-development/create-a-workflow-with-elevated-permissions-by-using-the-sharepoint-workflo

SHAREPOINT GROUPS

AIF Access & Reminder Workflow Access
- About Me: Contains SharePoint Groups used by the AIF Access & Reminder Workflow Access column.
- Who can view the membership of the group? Everyone
- Who can edit the membership of the group? Group Owner
- Members: Add all AD groups under COCL.local/COCL/Groups/Access Groups/AIF

AIF Access & Reminder Workflow Notify
- About Me: Contains SharePoint Groups used by the AIF Access & Reminder Workflow Notify column.
- Who can view the membership of the group? Everyone
- Who can edit the membership of the group? Group Owner
- Members: CMS-IntSec; CS-IntSec; IS-IntSec; PS-IntSec

SHAREPOINT COLUMNS

Contract End Date / Automatic Extension Deadline / Reporting Deadline / Insurance End Date
- The type of information in this column is: Date and Time
- Put this site column into: Existing group: Custom Columns
- Require that this column contains information: No
- Enforce unique values: No
- Date and Time Format: Date Only
- Display Format: Friendly
- Default value: (None)

Notify / Access
- The type of information in this column is: Person or Group
- Put this site column into: Existing group: Custom Columns
- Require that this column contains information: No
- Enforce unique values: No
- Allow multiple selections: Yes
- Allow selection of: People and Groups.
- Choose from: All Users
- Show field: Name (with presence)

SHAREPOINT CONTENT TYPES

Agreements in Force Document Set
- Note: This Content Type is on the Content Type Hub
- Add columns: Contract End Date, Automatic Extension Deadline, Reporting Deadline, Notify, Access
- Change the Notify column on The Dock to choose from: SharePoint Group: AIF Access & Reminder Workflow Notify
- Change the Access column on The Dock to choose from: SharePoint Group: AIF Access & Reminder Workflow Access
- Republish the Content Type

Agreements in Force
- Note: This Content Type is on the Content Type Hub
- Add columns: Insurance End Date, Notify, Access
- Change the Notify column on The Dock to choose from: SharePoint Group: AIF Access & Reminder Workflow Notify
- Change the Access column on The Dock to choose from: SharePoint Group: AIF Access & Reminder Workflow Access
- Republish the Content Type

AIF Access and Reminder Workflow Task
- Select parent content type from: List Content Types
- Parent Content Type: Workflow Task (SharePoint 2013)
- Put this site content type into: Existing group: Custom Content Types
- Set all columns to Hidden except: Related Items, Task Status, Task Outcome
- Add Default Value: Rejected to Task Outcome column

SHAREPOINT LISTS

AIF Access & Reminder Workflow Tasks
- List type: Tasks
- Add "AIF Access & Reminder Workflow Task" Content Type
- Remove Task Content Type
- Remove Workflow Task (SharePoint 2013) Content Type after you build the workflow
- Advanced Settings: Allow management of content types?: No
- Remove all views except All Tasks

SHAREPOINT ACCESS

// SKIP
// AIF.AIF Access & Reminder Workflow Tasks
// - AIF Access & Reminder Workflow Access, Read

AIF.Agreements
- AIF Access & Reminder Workflow Access, Read

AIF
- AIF Access & Reminder Workflow Access, Restricted Interfaces for Translation

WORKFLOW CONFIGURATION

Settings
- Task List: AIF Access & Reminder Workflow Tasks
- Workflow History: Workflow History
- Automatically update the workflow status to the current stage name: No
- Allow this workflow to me manually started: Check
- Start workflow automatically when an item is created: Yes
- Start workflow automatically when an item is changed: No

Notes
- In SharePoint Designer, if the workflow is switched between Visual Designer and Text-Based Designer, all parallel blocks will need to have their CompletionCondition reset.
