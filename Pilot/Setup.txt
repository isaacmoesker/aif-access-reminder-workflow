* Setup additions to the Prototype *

SHAREPOINT GROUPS

AIF Access & Reminder Workflow Notify
- About Me: Contains SharePoint Groups used by the AIF Access & Reminder Workflow Notify column.
- Members: CMS-IntSec, CS-IntSec, IS-IntSec, PS-IntSec
- Who can view the membership of the group? Everyone

SHAREPOINT COLUMNS

Contract End Date / Automatic Extension Deadline / Reporting Deadline / Insurance End Date
- Type: Date and Time
- Group: Custom Columns
- Require that this column contains information: No
- Enforce unique values: No
- Date and Time Format: Date Only
- Display Format: Friendly
- Default value: (None)

Notify / Access
- Type: Person or Group
- Group: Custom Columns
- Require that this column contains information: No
- Enforce unique values: No
- Allow multiple selections: Yes
- Allow selection of: People and Groups
- Choose from: All Users
- Show field: Name with Presence

SHAREPOINT CONTENT TYPES

Agreements in Force Document Set
- Add Contract End Date / Automatic Extension Deadline / Reporting Deadline
- Republish the Content Type
- Change the Notify column to choose from: SharePoint Group: AIF Access & Reminder Workflow Notify
- Change the Access column to choose from: SharePoint Group: AIF Access & Reminder Workflow Access

Agreements in Force
- Add Insurance End Date
- Republish the Content Type
- Change the Notify column to choose from: SharePoint Group: AIF Access & Reminder Workflow Notify
- Change the Access column to choose from: SharePoint Group: AIF Access & Reminder Workflow Access

SHAREPOINT LISTS

Page
- Name: Agreements
- Access: AIF Workflow Access Groups, Read

Content Query
- Show items from the following list: /corporate/legislative/AIF/Agreements
- Content Type: Custom Content Types: Agreements in Force
- Sort items by: Name
- Show items in ascending order.
- Limit the number of items to display: Uncheck
- Item style: Title, description, and document icon
- Title: Your Agreements

Intermediate Secretary Tasks
- Description: A Tasks list used by the Intermediate Secretary and the AIF Access and Reminder Workflow.
- Add AIF Workflow Tasks content type
- Advanced Settings: Allow management of content types?: No
- Remove Department access to the list
- Grant Intermediate Secretary contribute access to the list
- Remove all views except All Tasks

WORKFLOW

Settings
- Check: Start this workflow automatically when and item is created
